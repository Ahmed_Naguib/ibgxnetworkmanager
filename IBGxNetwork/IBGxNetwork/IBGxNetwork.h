//
//  IBGxNetwork.h
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IBGxNetwork.
FOUNDATION_EXPORT double IBGxNetworkVersionNumber;

//! Project version string for IBGxNetwork.
FOUNDATION_EXPORT const unsigned char IBGxNetworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IBGxNetwork/PublicHeader.h>


#ifndef _IBGxNetworkManager_
#define _IBGxNetworkManager_

#import "IBGxNetworkManager.h"
#import "UIImageView+IBGxNetwork.h"


#endif

