//
//  IBGxNetworkManager.h
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 HTTPRequestTypes ENUM.
 */
typedef NS_ENUM(NSUInteger,IBGxHTTPRequestType){
    
    IBGxHTTPRequestTypeGET,
    IBGxHTTPRequestTypePOST,
    IBGxHTTPRequestTypePUT,
    IBGxHTTPRequestTypePATCH,
    IBGxHTTPRequestTypeDELETE
};


@interface IBGxNetworkManager : NSObject

/**
 Returns the shared 'IBGxNetworkManager' object.
 */
+ (instancetype)sharedManager;


/**
 Creates an `NSURLSessionDataTask` instance with the request and excutes it.
 
 @param URLString The URL String for the request.
 @param requestType The HTTP Request Type for the request.
 @param parameters The 'NSDictionary' passed parameters to the request.
 @param headers The 'NSDictionary' headers of the request.
 @param success A block object to be executed when the task is finished successfully. This block has no return value and takes one argument: the response data.
 @param failure A block object to be executed when the task is finishes with errors. This block has no return value and takes one argument: and the error describing the network that occurred.
 */
- (NSURLRequest *)makeRequestWithURLString:(NSString *)URLString
                               requestType:(IBGxHTTPRequestType)requestType
                                parameters:(NSDictionary *)parameters
                                   headers:(NSDictionary *)headers
                                   success:(void (^)(NSData *response))success
                                   failure:(void (^)(NSData *response, NSError *error))failure;


@end

