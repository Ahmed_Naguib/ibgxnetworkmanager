//
//  IBGxNetworkManager.m
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import "IBGxNetworkManager.h"
#import "IBGxURLSessionHandler.h"

@implementation IBGxNetworkManager


#pragma mark - Initialization

+ (instancetype)sharedManager {
    
    static IBGxNetworkManager *sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


#pragma mark - Requests

- (NSURLRequest *)makeRequestWithURLString:(NSString *)URLString
                               requestType:(IBGxHTTPRequestType)requestType
                                parameters:(NSDictionary *)parameters
                                   headers:(NSDictionary *)headers
                                   success:(void (^)(NSData *response))success
                                   failure:(void (^)(NSData *response, NSError *error))failure {
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.HTTPMethod = [self getRequestTypeStringForRequestType:requestType];
    
    if(requestType == IBGxHTTPRequestTypeGET)
    {
        NSString *post = @"?";
        for (NSString *key in parameters){
            post = [post stringByAppendingString:[NSString stringWithFormat:@"%@=%@&", key, [parameters valueForKey:key]]];
        }
        post = [post substringToIndex:post.length-1];
        
        NSURL *url =[NSURL URLWithString:[[URLString stringByAppendingString:post]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        [request setURL:url];
    } else{
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
        [request setURL: [NSURL URLWithString:URLString]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
        [request setHTTPBody:jsonData];
    }
    
    // Add Headers
    for (NSString *key in headers) {
        NSString *value = [headers objectForKey:key];
        [request setValue:value forHTTPHeaderField:key];
    }
    
    // Send Request
    [self sendHTTPRequest:request success:success failure:failure];
    return request;
}


#pragma mark - RequestsHelperFunctions

- (NSString *) getRequestTypeStringForRequestType:(IBGxHTTPRequestType)requestType{
    
    switch (requestType) {
            
        case IBGxHTTPRequestTypePOST:
            return @"POST";
        case IBGxHTTPRequestTypePUT:
            return @"PUT";
        case IBGxHTTPRequestTypeDELETE:
            return @"DELETE";
        case IBGxHTTPRequestTypePATCH:
            return @"PATCH";
        default:
            return @"GET";
    }
}

- (void)sendHTTPRequest:(NSURLRequest *)request
                success:(void (^)(NSData *response))success
                failure:(void (^)(NSData *response, NSError *error))failure {
    
    [self createHTTPRequest:request withCompletion:^(NSData *response, NSError *error) {
        if (! error) {
            if (success) {
                success(response);
            }
        } else {
            if (failure) {
                failure(response, error);
            }
        }
    }];
}


- (void)createHTTPRequest:(NSURLRequest *)request
           withCompletion:(void (^)(NSData *response, NSError *error))completion {
    
    [[IBGxURLSessionHandler sharedInstance] makeRequest:request withCompletion:completion];
}

@end