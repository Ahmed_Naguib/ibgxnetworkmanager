//
//  IBGxURLSessionHandler.h
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBGxURLSessionHandler : NSObject

/**
 The 'NSURLSession' used in all requests.
 */
@property (nonatomic) NSURLSession *session;

/**
 The 'NSLock' used to maintain thread-safe requests.
 */
@property (nonatomic) NSLock *lock;

/**
 The 'NSMutableArray' that hold a queue of all tasks.
 */
@property (nonatomic) NSMutableArray<NSURLSessionTask*> *tasksQueue;


/**
 Returns the shared 'IBGxURLSessionManager' object.
 */
+ (instancetype)sharedInstance;

/**
 Creates an 'NSURLSessionDataTask' instance with the request and excutes it.
 
 @param request 'The NSURLRequest' used to make the request.
 @param completion A block object to be executed when the task finishes. This block has no return value and takes two arguments: the response data and the error describing the network error that occurred.
 */
- (void)makeRequest:(NSURLRequest *)request withCompletion:(void (^)(NSData *responseData, NSError *error))completion;

@end