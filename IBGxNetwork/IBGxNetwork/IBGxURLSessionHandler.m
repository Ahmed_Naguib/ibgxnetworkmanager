//
//  IBGxURLSessionHandler.m
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import "IBGxURLSessionHandler.h"
#import "Reachability/Reachability.h"

@implementation IBGxURLSessionHandler


#pragma mark - Constants

static const NSInteger IBGxNetworkMaxConcurrentOperationCountForWIFI = 6;
static const NSInteger IBGxNetworkMaxConcurrentOperationCountForCellular = 2;

static NSString *const IBGxLockName = @"IBGxNetwork.lock";
static NSString *const IBGxBackgroundSessionConfigurationIdentifier = @"IBGxNetwork.backgroundSessionConfigurationIdentifier";


#pragma mark - Initialization

- (instancetype)init {
    
    return [self initWithSessionConfiguration:nil];
}

- (instancetype)initWithSessionConfiguration:(NSURLSessionConfiguration  *)sessionConfiguration{
    self = [super init];
    if (self) {
        self.lock = [[NSLock alloc] init];
        self.lock.name = IBGxLockName;
        self.tasksQueue = [[NSMutableArray alloc] init];
        
        if (sessionConfiguration == nil){
            sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            // to handle requests when app is suspended - requires delegation -
            // sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:IBGxBackgroundSessionConfigurationIdentifier];
        }
    
        sessionConfiguration.HTTPMaximumConnectionsPerHost = IBGxNetworkMaxConcurrentOperationCountForWIFI;
        self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    }
    return self;
}


+ (instancetype)sharedInstance {
    
    static IBGxURLSessionHandler *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}


#pragma mark - Requests

- (void)makeRequest:(NSURLRequest *)request withCompletion:(void (^)(NSData* responseData, NSError *error))completion {
    
    [self.lock lock];
    [self setNetworkMaxConcurrentOperation];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self.lock lock];
        [self.tasksQueue removeObject:self.tasksQueue.firstObject];
        [self.tasksQueue.firstObject resume];
        if (completion) {
            completion(data, error);
        }
        [self.lock unlock];
    }];
    
    [self.tasksQueue addObject:task];
    [self.tasksQueue.firstObject resume];
    [self.lock unlock];
}


#pragma mark - NetworkConditions

-(NetworkStatus)setNetworkMaxConcurrentOperation
{
    NetworkStatus remoteHostStatus = [[Reachability  reachabilityForInternetConnection] currentReachabilityStatus];
    if (remoteHostStatus == ReachableViaWWAN){
        self.session.configuration.HTTPMaximumConnectionsPerHost =IBGxNetworkMaxConcurrentOperationCountForCellular;
    }
    else if (remoteHostStatus == ReachableViaWiFi){
        self.session.configuration.HTTPMaximumConnectionsPerHost =IBGxNetworkMaxConcurrentOperationCountForWIFI;
    }
    
    return remoteHostStatus;
}

@end