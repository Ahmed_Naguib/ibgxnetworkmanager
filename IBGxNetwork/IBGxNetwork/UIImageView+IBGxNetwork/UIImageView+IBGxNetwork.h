//
//  UIImageView+IBGxNetwork.h
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (IBGxNetwork)

/**
 Set image to the imageView from URL
 
 @param URLString The URL String of the image.
 */
- (void) setImageWithURL:(NSString *)urlString;


/**
 Set image to the imageView from URL
 
 @param URLString The URL String of the image.
 @param withSpinner Bool value to indicate showing activity indicator when loading the image or not.
 */
- (void) setImageWithURL:(NSString *)urlString withSpinner:(BOOL)withSpinner;


/**
 Set image to the imageView from URL
 
 @param URLString The URL String of the image.
 @param completionBlock A block object to be executed when the image is finished downloading the image from the url. This block has no return value and takes 1 argument: the downloaded image
 */
- (void) setImageWithURL:(NSString *)urlString completionBlock:(void (^)(UIImage *image))completionBlock;



/**
 Set image to the imageView from URL
 
 @param URLString The URL String of the image.
 @param withSpinner Bool value to indicate showing activity indicator when loading the image or not.
 @param completionBlock A block object to be executed when the image is finished downloading the image from the url. This block has no return value and takes 1 argument: the downloaded image
 */
- (void) setImageWithURL:(NSString *)urlString withSpinner:(BOOL)withSpinner completionBlock:(void (^)(UIImage *image))completionBlock;

@end
