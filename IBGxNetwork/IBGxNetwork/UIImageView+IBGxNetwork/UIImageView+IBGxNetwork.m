//
//  UIImageView+IBGxNetwork.m
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import "UIImageView+IBGxNetwork.h"
#import "IBGxNetworkManager.h"

@implementation UIImageView (IBGxNetwork)


#pragma mark - ExtendedMethods

- (void) setImageWithURL:(NSString *)urlString{
    
    [self setImageWithURL:urlString withSpinner:NO completionBlock:nil];
}
- (void) setImageWithURL:(NSString *)urlString withSpinner:(BOOL)withSpinner{
    
    [self setImageWithURL:urlString withSpinner:withSpinner completionBlock:nil];
}

- (void) setImageWithURL:(NSString *)urlString completionBlock:(void (^)(UIImage *image))completionBlock {
    
    [self setImageWithURL:urlString withSpinner:NO completionBlock:completionBlock];
}

- (void) setImageWithURL:(NSString *)urlString withSpinner:(BOOL)withSpinner completionBlock:(void (^)(UIImage *image))completionBlock {
    
    if (withSpinner){
        [self showSpinner];
    }
    
    [[IBGxNetworkManager sharedManager] makeRequestWithURLString:urlString requestType:IBGxHTTPRequestTypeGET parameters:nil headers:nil success:^(NSData *response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (withSpinner){
                [self hideSpinner];
            }
            
            UIImage *downloadedImage = [[UIImage alloc] initWithData:response];
            if (completionBlock){
                completionBlock(downloadedImage);
            }else{
                self.image = downloadedImage;
            }
            NSLog(@"Excute setImageFromURL Succeed");
        });
        
    } failure:^(NSData *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (withSpinner){
                [self hideSpinner];
            }
            
            if (completionBlock){
                completionBlock(nil);
            }else{
                self.image = nil;
            }
            
        });
        
        NSLog(@"Excute setImageFromURL Failed %@" , error.localizedDescription);
    }];
    
}


#pragma mark - HelperMethods

-(void) showSpinner{
    
    if (![self.subviews.lastObject isKindOfClass:[UIActivityIndicatorView class]]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setNeedsDisplay];
            [self setNeedsLayout];
            
            UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            loadingIndicator.color = [UIColor blackColor];
            loadingIndicator.center = self.center;
            loadingIndicator.hidesWhenStopped = YES;
            [self addSubview:loadingIndicator];
            [loadingIndicator startAnimating];
        });
    }
}

-(void) hideSpinner{
    
    if ([self.subviews.lastObject isKindOfClass:[UIActivityIndicatorView class]]){
        [self.subviews.lastObject stopAnimating];
        [self.subviews.lastObject removeFromSuperview];
    }
}
@end
