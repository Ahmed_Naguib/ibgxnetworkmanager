//
//  ViewController.m
//  IBGxNetwork
//
//  Created by Ahmed on 5/14/16.
//  Copyright © 2016 Ahmed. All rights reserved.
//

#import "ViewController.h"
#import "IBGxNetwork/IBGxNetwork.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController


#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self excuteHTTPGetRequest];
    [self excuteHTTPPostRequest];
    [self excuteImageViewWithURL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


#pragma mark - IBGxDemo

- (void) excuteHTTPGetRequest{
    
    NSString *url = @"http://httpbin.org/get";
    NSDictionary *params = @{
                             @"firstname":@"Ahmed",
                             @"lastname" :@"Naguib"
                            };
    
    IBGxNetworkManager *networkManager = [IBGxNetworkManager sharedManager];
    [networkManager makeRequestWithURLString:url
                                 requestType:IBGxHTTPRequestTypeGET
                                  parameters:params
                                     headers:nil
                                     success:^(NSData *response) {
                                         
                                         NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:nil];
                                         NSLog(@"Excute GET Succeed %@" , dictionary);
                                         
                                     }
                                     failure:^(NSData *response, NSError *error) {
                                         
                                         NSLog(@"Excute GET Failed %@" , error.localizedDescription);
                                     }];
}


- (void) excuteHTTPPostRequest{
    
    NSString *url = @"http://httpbin.org/post";
    NSDictionary *params = @{
                             @"firstname":@"Ahmed",
                             @"lastname" :@"Naguib"
                            };
    
    IBGxNetworkManager *networkManager = [IBGxNetworkManager sharedManager];
    [networkManager makeRequestWithURLString:url
                                 requestType:IBGxHTTPRequestTypePOST
                                  parameters:params
                                     headers:nil
                                     success:^(NSData *response) {
                                         
                                         NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:nil];
                                         NSLog(@"Excute POST Succeed %@" , dictionary);
                                         
                                     }
                                     failure:^(NSData *response, NSError *error) {
                                         
                                         NSLog(@"Excute POST Failed %@" , error.localizedDescription);
                                     }];
}


- (void) excuteImageViewWithURL{
    
    NSString *url = @"http://googansolutions.com/naqel/media/country/513-saudiarabia.gif";
    [self.imageView setImageWithURL:url withSpinner:YES];
}

@end
